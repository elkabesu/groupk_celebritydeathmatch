package gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.Gdx;

public class ActorAI extends GameObject implements Updatable{
	public float actorHP = 250;
	public float actorMP = 100;
	private int exp;
	public int currentLevel = 1;
	private int maxLevel = 10;
	private int nextLevelXP = 100;

	float deltaTime = Gdx.graphics.getDeltaTime();

	public ActorAI(TextureRegion tex, int w, int h){
		//position = new Vector2(0,0);
		sprite = new Sprite(tex);
		sprite.setOrigin(tex.getRegionWidth()/2, tex.getRegionHeight()/2);
		setIsDrawable(false);
		sprite.setPosition(4840, 2723);
	}

	@Override
	public void update(float deltaTime) {
		sprite.setPosition(sprite.getX() - (20 *1.6f) * deltaTime, sprite.getY() - (20 * 0.9f) * deltaTime);
	}

}