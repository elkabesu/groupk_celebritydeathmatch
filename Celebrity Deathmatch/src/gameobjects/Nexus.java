package gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class Nexus extends GameObject implements Updatable{

	public int sightRadius = 50;

	public Nexus(Texture texture){
		sprite = new Sprite(texture);

		setHP(2000);

		sprite.setOrigin(texture.getWidth()/2, texture.getHeight()/2);
		sprite.setSize(100, 100);
		setIsDrawable(true);
		setIsUpdatable(true);
		
	}

	@Override
	public void update(float deltaTime) {
		
	}

}
