package gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.Gdx;

import game.Constants;

public class Actor extends GameObject implements Updatable{
	float actorSpeed;
	float cosTheta; 
	double degree;
	private Vector2 direction;
	private Vector2 position;
	private Vector2 targetDirection;
	public float actorHP = 250;
	public float actorMP = 100;
	private int exp;
	public int currentLevel = 1;
	private int maxLevel = 10;
	private int nextLevelXP = 100;

	float deltaTime = Gdx.graphics.getDeltaTime();

	public Actor(TextureRegion tex, int w, int h){
		//position = new Vector2(0,0);
		sprite = new Sprite(tex);
		sprite.setOriginCenter();
		setIsDrawable(false);
		direction = new Vector2(0, -1);
		targetDirection = new Vector2(0, -1);
		actorSpeed = Constants.ACTOR_BASE_SPEED;
		position = new Vector2(actorSpeed, actorSpeed);
	}

	@Override
	public void update(float deltaTime) {
		cosTheta = direction.dot(targetDirection)/targetDirection.len();
		if(cosTheta>1)
			cosTheta = 1;
		
		degree = Math.acos(cosTheta);
		if(direction.crs(targetDirection)>0)
			degree = -degree;
		
		degree = Math.toDegrees(degree);
		//sprite.rotate((float) degree);
		direction.rotate((float) -degree);
		sprite.setPosition(position.x , position.y );
		
		if(actorHP <= 0)
			actorHP = 0;
		if(actorMP <= 0)
			actorMP = 0;
		
		//System.out.println(sprite.getX()+ " "+sprite.getX());
	}
	
	public void moveTo(){
		position.x += direction.x * actorSpeed ;
		position.y -= direction.y * actorSpeed ;
	}	
	
	public void face(Vector2 targetPos){
		targetDirection = targetPos;
	}
	
	public Vector2 getPosition(){
		return new Vector2(sprite.getX(), sprite.getY());
	}
	
	public Vector2 getDirection(){
		return direction;
	}
	
	public void gainExp(int ex){
		
		if(currentLevel <= maxLevel){
			exp += ex;
			if(exp>=nextLevelXP){
				currentLevel++;
				if(currentLevel>=10){
					currentLevel=10;
				}
			}
		}


	}
	
	public void getTargetDirection(){
		System.out.println("Target Direction: " + targetDirection);
	}
	
	public float length(){
		return targetDirection.len();
	}
	
	public void changeSpeed(float aS){
		actorSpeed = aS;
	}
	
	public void respawn(){
		actorHP = 250;
		actorMP = 100;
		position.x = 0;
		position.y = 0;
	}
}