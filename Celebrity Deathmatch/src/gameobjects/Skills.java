package gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import game.Controller;

public class Skills extends GameObject implements Updatable {

	public Skills (Texture texture, float x, float y){
		sprite = new Sprite(texture);
		sprite.setOrigin(texture.getWidth()/2,texture.getHeight()/2);
		sprite.setPosition(x - 37.5f, y -37.5f);
		setIsDrawable(true);
	}
	
	@Override
	public void update(float deltaTime) {

	}
}
