package gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import game.Constants;

public class Creep extends GameObject implements Updatable{
	public int sightRadius = 50;
	public int attackRadius = 10;
	public float creepSpeed = 1;
	
	public Creep(Texture texture){
		sprite = new Sprite(texture);
		setHP(100);
		creepSpeed = Constants.CREEP_BASE_SPEED;
		sprite.setOrigin(texture.getWidth()/2, texture.getHeight()/2);
		sprite.setSize(50, 50);
		setIsDrawable(true);
		setIsUpdatable(true);
	}
	@Override
	public void update(float deltaTime) {
		float x = sprite.getX();
		float y = sprite.getY();
		//if(x > 100 && y > 100)
		sprite.setPosition(x - (creepSpeed *1.6f) * deltaTime, y - (creepSpeed * 0.9f) * deltaTime);
	}
	
	public Vector2 getPosition(){
		return new Vector2(sprite.getX(), sprite.getY());
	}
	
	public void changeSpeed(int cS){
		creepSpeed = cS;
	}
	
	public float creepSpeed(){
		return creepSpeed;
	}
}