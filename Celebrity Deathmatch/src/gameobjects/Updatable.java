package gameobjects;

public interface Updatable {
	void update(float deltaTime);
}
