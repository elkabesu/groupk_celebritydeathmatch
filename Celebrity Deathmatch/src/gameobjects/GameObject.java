package gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;

public abstract class GameObject {
    private boolean drawable;
    private boolean updatable;
    public Sprite sprite; 
    private float HP;
    private float MP;
    private boolean ally;
    
    public GameObject(){
    	
    }
    
    public boolean isDrawable(){
        return drawable;
    }
    public boolean isUpdatable(){
        return updatable;
    }
    public void setIsDrawable(boolean val){
        drawable = val;
    }
    public void setIsUpdatable(boolean val){
        updatable = val;
    }
    public float getHP(){
    	return HP;
    }
    public void setHP(float hp){
    	HP = hp;
    }
    public void updateHP(float hp){
    	HP = HP - hp;
    }
    public float getMP(){
    	return MP;
    }
    public void setMP(float mp){
    	MP = mp;
    }
    public void setIsTeam(boolean team){		// Setting teams, can think of as is this object an enemy. True means ally false means enemy
    	ally = team;
    }
    public boolean getTeam(){
    	return ally;
    }
}

