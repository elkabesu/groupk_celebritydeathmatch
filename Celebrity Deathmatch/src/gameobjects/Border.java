package gameobjects;

import org.lwjgl.util.Color;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.Graphics;

public class Border extends GameObject implements Updatable{


	public Border(int startX, int startY, int endX, int endY){
		Pixmap l = new Pixmap(2000, 2000, Format.RGBA4444);
		l.setColor(155511);
		l.drawLine(startX, startY, endX, endY);
		sprite = new Sprite(new Texture(l));
		sprite.setOrigin(1000,1000);
		sprite.setPosition(0,0);
		setIsDrawable(true);
	}
	
	@Override
	public void update(float deltaTime) {
		
	}

}
