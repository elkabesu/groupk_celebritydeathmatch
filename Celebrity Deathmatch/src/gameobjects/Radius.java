package gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

import game.Controller;
import gameobjects.Creep;

public class Radius extends GameObject implements Updatable {
	
	public int r;
	private float _x, _y;
	private Creep creep;
	
	public Radius (Texture texture, float x ,float y, int r){
		sprite = new Sprite(texture);
		sprite.setOrigin(texture.getWidth()/2,texture.getHeight()/2);
		sprite.setPosition(x, y);
		this._x = x;
		this._y = y;
		this.r = r;
		setIsDrawable(true);
		
	}
	
	@Override
	public void update(float deltaTime) {
		_x -= (creep.creepSpeed *1.6f) * deltaTime;
		_y -= (creep.creepSpeed * 0.9f) * deltaTime;
		System.out.println(_x +" "+_y);
		sprite.setPosition(_x , _y );
	}
	
	public int getRadius(){
		return r;
	}

}