package gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class Turret extends GameObject implements Updatable{
	
	public int sightRadius = 50;
	public int attackRadius = 10;
	
	public Turret(Texture texture, int turnumb){
		sprite = new Sprite(texture);
		
		if(turnumb==4)
			setHP(2000);
		else if(turnumb==3)
			setHP(1500);
		else if(turnumb==2)
			setHP(1000);
		else if(turnumb==1)
			setHP(700);
		
		sprite.setOrigin(texture.getWidth()/2, texture.getHeight()/2);
		sprite.setSize(100, 100);
		setIsDrawable(true);
		setIsUpdatable(true);
	}

	@Override
	public void update(float deltaTime) {
		
	}

}