package game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.CDMGame;

public class WelcomeScreen implements Screen, InputProcessor{
	CDMGame game;				// Reference back to the game
	BitmapFont font;
	private SpriteBatch spriteBatch;
	private Texture characterSelectionScreen, instructionButton;
	private Texture neo, chuckNorris;
	private Music charScreenMusic;
	int screenWidth, screenHeight;
	
	public WelcomeScreen(CDMGame g){
		super();
		font = new BitmapFont();
		Gdx.input.setInputProcessor(this);						// Set input to listen to this screen
		spriteBatch = new SpriteBatch(); 
		game = g;
		screenWidth = Constants.GAME_WINDOW_WIDTH;
		screenHeight = Constants.GAME_WINDOW_HEIGHT;
		characterSelectionScreen = new Texture("temporarycharselectionscreen.jpg");	// Background image for character selection screen
		instructionButton = new Texture("Instruction-Button.gif");					// Instruction Button image
        neo = new Texture("neo.png");												// Character selection image for Neo
        chuckNorris = new Texture("chuck_norris.png");								// Character selection image for Chuck Norris
		charScreenMusic = Gdx.audio.newMusic(Gdx.files.internal("back.mp3"));		// Start playing the welcome screen music
		charScreenMusic.setLooping(true);
		charScreenMusic.play();
		charScreenMusic.setVolume(0.5f);
	}
	
	@Override
	public void dispose() {
		font.dispose();
		spriteBatch.dispose();
		characterSelectionScreen.dispose();
		instructionButton.dispose();
		neo.dispose();
		chuckNorris.dispose();
		charScreenMusic.dispose();
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void render(float arg0) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		spriteBatch.begin();
		spriteBatch.draw(characterSelectionScreen, 0, 0, screenWidth, screenHeight);					// Draw background image
    	font.draw(spriteBatch, "Welcome to Celebrity Deathmatch!", screenWidth/2 - 100, screenHeight);	// Draw text
        font.draw(spriteBatch, "Choose a character", screenWidth/2 - 60, screenHeight-20);				// Draw text
        spriteBatch.draw(instructionButton, 0, screenHeight - instructionButton.getHeight());			// Draw instructions button
        spriteBatch.draw(neo, screenWidth/8, screenHeight/4, 400, 400);									// Draw neo portrait
        spriteBatch.draw(chuckNorris, screenWidth/8 * 7 - 400, screenHeight/4, 400, 400);				// Draw chuckNorris portrait
		spriteBatch.end();
	}
	
	@Override
	public void resize(int arg0, int arg1) {
	}

	@Override
	public void resume() {
	}

	@Override
	public void show() {
	}

	@Override
	public boolean keyDown(int arg0) {
		return false;
	}

	@Override
	public boolean keyTyped(char arg0) {
		return false;
	}

	@Override
	public boolean keyUp(int arg0) {
		return false;
	}

	@Override
	public boolean mouseMoved(int arg0, int arg1) {
		return false;
	}

	@Override
	public boolean scrolled(int arg0) {
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int arg3) {
		if(Gdx.input.getX() > screenWidth/8 && Gdx.input.getX() < screenWidth/8 + 400 && Gdx.input.getY() > screenHeight/4 && Gdx.input.getY() < screenHeight/4 + 400){		// If Neo picture is clicked
        	game.startGame(true, false);
        	dispose();
		}
		else if(Gdx.input.getX() > screenWidth/8 * 7 - 400 && Gdx.input.getX() < screenWidth/8 * 7 && Gdx.input.getY() > screenHeight/4 && Gdx.input.getY() < screenHeight/4 + 400){	// If Chuck Norris picture is clicked
    		game.startGame(false, true);
    		dispose();
    	}
		else if(Gdx.input.getX() > 0 && Gdx.input.getX() < instructionButton.getWidth() && Gdx.input.getY() > 0 && Gdx.input.getY() < instructionButton.getHeight())	// If instruction button picture is clicked
			game.setScreen(new InstructionScreen(game));
		return false;
	}

	@Override
	public boolean touchDragged(int arg0, int arg1, int arg2) {
		return false;
	}

	@Override
	public boolean touchUp(int arg0, int arg1, int arg2, int arg3) {
		return false;
	}
	
	public void setIP(){				// Used to set input back to the welcome screen after switching screens to instructions and switching back
		Gdx.input.setInputProcessor(this);
	}
}