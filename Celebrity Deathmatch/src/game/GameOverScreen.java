package game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.CDMGame;

public class GameOverScreen implements Screen, InputProcessor{
	CDMGame game;
	BitmapFont font;
	private SpriteBatch spriteBatch;
	private Texture gameOver, replayButton, quitButton;
	int screenWidth, screenHeight;
	
	public GameOverScreen(CDMGame g, boolean win){
		super();
		font = new BitmapFont();
		Gdx.input.setInputProcessor(this);
		spriteBatch = new SpriteBatch(); 
		game = g;
		screenWidth = Constants.GAME_WINDOW_WIDTH;
		screenHeight = Constants.GAME_WINDOW_HEIGHT;
		if(win)
			gameOver = new Texture("victory-11.png");							// Victory image
		else
			gameOver = new Texture("defeat.png");								// Defeat image
		replayButton = new Texture("play_again_button.png");					// Replay Button image
		quitButton = new Texture("exit.png");									// Quit Button image
	}
	
	@Override
	public void dispose() {
		font.dispose();
		spriteBatch.dispose();
		gameOver.dispose();
		replayButton.dispose();
		quitButton.dispose();
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void render(float arg0) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		spriteBatch.begin();
		spriteBatch.draw(gameOver, screenWidth/2 - 250, screenHeight/2 - 150, 500, 500);
        spriteBatch.draw(replayButton, screenWidth/2 - 250 - 100, screenHeight/2 - 150 - 50, 200, 100);
        spriteBatch.draw(quitButton, screenWidth/2 + 250 - 50, screenHeight/2 - 150 - 50, 100, 100);
		spriteBatch.end();
	}
	
	@Override
	public void resize(int arg0, int arg1) {
	}

	@Override
	public void resume() {
	}

	@Override
	public void show() {
	}

	@Override
	public boolean keyDown(int arg0) {
		return false;
	}

	@Override
	public boolean keyTyped(char arg0) {
		return false;
	}

	@Override
	public boolean keyUp(int arg0) {
		return false;
	}

	@Override
	public boolean mouseMoved(int arg0, int arg1) {
		return false;
	}

	@Override
	public boolean scrolled(int arg0) {
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int arg3) {
		if(Gdx.input.getX() > screenWidth/2 - 250 - 100 && Gdx.input.getX() < screenWidth/2 - 250 - 100 + 200 && Gdx.input.getY() > screenHeight/2 - (-1 * (150 - 50)) && Gdx.input.getY() < screenHeight/2 - (-1 * (150 - 50 + 100))){		// If replay button is clicked
        	game.setWelcomeScreen();
        	dispose();
		}
		else if(Gdx.input.getX() > screenWidth/2 + 250 - 50 && Gdx.input.getX() < screenWidth/2 + 250 - 50 + 100 && Gdx.input.getY() > screenHeight/2 - (-1 * (150 - 50)) && Gdx.input.getY() < screenHeight/2 - (-1 * (150 - 50 + 100))){	// If quit button is clicked
    		Gdx.app.exit();
    	}
		return false;
	}

	@Override
	public boolean touchDragged(int arg0, int arg1, int arg2) {
		return false;
	}

	@Override
	public boolean touchUp(int arg0, int arg1, int arg2, int arg3) {
		return false;
	}
	
	public void setIP(){
		Gdx.input.setInputProcessor(this);
	}
}
