package game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class RenderActor {
	Texture texture;
	Texture textureAura;
	TextureRegion [] actorFrames;
	TextureRegion [] auraFrames;

	public RenderActor(int num){
		if(num == 1)
			texture = new Texture(Gdx.files.internal("wingchar.png"));
		if(num == 2)
			texture = new Texture(Gdx.files.internal("neochar.png"));
		
		TextureRegion [][] tmp = TextureRegion.split(texture, texture.getWidth()/4, texture.getHeight()/4);
		actorFrames = new TextureRegion[16];
		int index = 0;
		for(int i=0; i<4; i++){
			for(int j = 0; j<4; j++){
				actorFrames[index++] = tmp[i][j];
			}
		}
		
		RenderAura(num);
	}
	
	public void RenderAura(int num){
		if(num == 1)
			textureAura = new Texture(Gdx.files.internal("auraB.png"));
		if(num == 2)
			textureAura = new Texture(Gdx.files.internal("auraG.png"));
	
		TextureRegion [][] tmp2 = TextureRegion.split(textureAura, texture.getWidth()/4, texture.getHeight()/4);
		auraFrames = new TextureRegion[16];
		int index = 0;
		for(int i=0; i<4; i++){
			for(int j = 0; j<4; j++){
				auraFrames[index++] = tmp2[i][j];
			}
		}
	}
	
}


