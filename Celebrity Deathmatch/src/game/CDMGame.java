package game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

public class CDMGame extends Game {
	private Screen WelcomeScreen;
	private Screen GameScreen;
	

    @Override
    public void create () {
    	WelcomeScreen = new WelcomeScreen(this);
    	setScreen(WelcomeScreen);
    }

    @Override
    public void render () {
    	getScreen().render(Gdx.graphics.getDeltaTime());
    }

    public void startGame(boolean Neo, boolean ChuckNorris){
    	GameScreen = new GameScreen(this, Neo, ChuckNorris);
    	setScreen(GameScreen);
    }
    
    public void setWelcomeScreen(){
    	WelcomeScreen = new WelcomeScreen(this);
    	setScreen(WelcomeScreen);
    }
    
    public Screen getWelcomeScreen(){
    	return WelcomeScreen;
    }
    
    public Screen getGameScreen(){
    	return GameScreen;
    }
    
    @Override
    public void dispose(){
    	getScreen().dispose();
    }

}
