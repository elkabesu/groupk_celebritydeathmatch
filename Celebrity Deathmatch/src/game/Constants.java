package game;

public class Constants {
	public static final int GAME_WINDOW_WIDTH = 1280;
	public static final int GAME_WINDOW_HEIGHT = 720;
	public static final int ACTOR_BASE_SPEED = 5;
	public static final int CREEP_BASE_SPEED = 20;
	public static final int ACTOR_WIDTH = 100;				// To be changed when we have a better idea about scalability
	public static final int ACTOR_HEIGHT = 100;				// To be changed when we have a better idea about scalability
	public static final int CREEP_WIDTH = 5;				// To be changed when we have a better idea about scalability
	public static final int CREEP_HEIGHT = 5;			// To be changed when we have a better idea about scalability
	public static final int INHIBITOR_WIDTH = 15;		// To be changed when we have a better idea about scalability
	public static final int INHIBITOR_HEIGHT = 15;		// To be changed when we have a better idea about scalability
	public static final int NEXUS_WIDTH = 20;;			// To be changed when we have a better idea about scalability
	public static final int NEXUS_HEIGHT = 20;			// To be changed when we have a better idea about scalability
	public static final int TURRET_WIDTH = 10;			// To be changed when we have a better idea about scalability
	public static final int TURRET_HEIGHT = 20;			// To be changed when we have a better idea about scalability
	public static final int MAP_WIDTH = 6400;
	public static final int MAP_HEIGHT = 3600;
}
