package game;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

import gameobjects.Actor;
import gameobjects.ActorAI;
import gameobjects.Border;
import gameobjects.GameObject;
import gameobjects.Creep;
import gameobjects.Inhibitor;
import gameobjects.Nexus;
import gameobjects.Turret;
import gameobjects.Skills;
import gameobjects.Radius;

public class Controller {
	ArrayList<GameObject> drawableObjects;
	RenderActor actorPic;
	RenderActor actorAIPic;
	Actor actor;
	ActorAI actorAI;
	private Music backgroundNoise;
	private Sound footSound;
	
	private float deltaT;
	private float screenWidth, screenHeight;
	private float skillTimer;
	private int num;
	boolean switchedScreen;
	boolean isDead, noMana;
	boolean skillHit, aiSkill;
	boolean gameOver, victory, defeat;
	double angle;
	public Radius attackingRadius, sightRadius;
	private GameScreen camera;
	float camX, camY, camZ;
	private float newMouseX, newMouseY;
	private int creepNum;
	//private ArrayList<GameObject> creeps;
	//private ArrayList<ArrayList<GameObject>> creepList;
	
	public Controller(GameScreen cam){
		drawableObjects = new ArrayList<GameObject>();
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		skillTimer = 0;
		creepNum = 5;
		aiSkill = false;
		isDead = false;
		noMana = false;
		gameOver = false;
		victory = false;
		defeat = false;
		camera = cam;
		camX = 0;
		camY = 0;
		camZ = 0;
		initCreep(false, 3840, 2160);
		//initLanes();
		initInhibitor();
		initNexus();
		initTurret();
		initSound();
	}
	
	private void initActor(){
		int w = Constants.ACTOR_WIDTH;
		int h = Constants.ACTOR_HEIGHT;
        actor = new Actor(actorPic.actorFrames[0], w, h);
        //actor.setIsTeam(true);
        drawableObjects.add(actor);
	}
	
	private void initActorAI(){
		int w = Constants.ACTOR_WIDTH;
		int h = Constants.ACTOR_HEIGHT;
        actorAI = new ActorAI(actorAIPic.actorFrames[0], w, h);
        //actorAI.setIsTeam(false);
        drawableObjects.add(actorAI);
	}
	// Commented out non working code for now to allow game to be runnable
	
	private void initCreep(boolean ally, float startPositionX, float startPositionY){
		// Calling initCreep will spawn a new wave
		// Spawn 5 Melee creeps and 1 Range creep
		/*for(int i = 0; i < 5; i++){
			Creep creep = new Creep(new Texture("creep.png"));		// Need picture of melee creep
			creep.sprite.setPosition();				// Need to set positions of 5 melee creeps
			creep.sprite.setOrigin(Constants.creepWidth/2, Constants.creepHeight/2);
			creep.setRotVel();		// need to set rotation
			drawableObjects.add(creep);
		}*/
		//creeps = new ArrayList<GameObject>();
		for(int i = 0; i < creepNum; i++){
			//Creep creep = new Creep(new Texture("creep.png"));
			Creep creep = new Creep(new Texture("creep.png"));
			creep.setIsTeam(ally);
			creep.sprite.setPosition(startPositionX + (100 * i), startPositionY + (56.25f * i));
			creep.sprite.setOrigin(Constants.CREEP_WIDTH/2, Constants.CREEP_HEIGHT/2);
			//creeps.add(creep);
			/*
			Pixmap attackCircle = new Pixmap(101, 101, Pixmap.Format.RGBA4444);
			attackCircle.setColor(Color.RED);
			attackCircle.drawCircle(50,50,50);
			attackingRadius = new Radius(new Texture(attackCircle),creep.sprite.getX(), creep.sprite.getY(), 50);
			creeps.add(attackingRadius);
			Pixmap sightCircle = new Pixmap(401, 401, Pixmap.Format.RGBA4444);
			sightCircle.setColor(Color.ORANGE);
			sightCircle.drawCircle(200,200,200);
			sightRadius = new Radius(new Texture(sightCircle),creep.sprite.getX(), creep.sprite.getY(), 200);
			creeps.add(sightRadius);
			*/
			drawableObjects.add(creep);
			//drawableObjects.add(attackingRadius);
			//drawableObjects.add(sightRadius);
		}
		
		
		/*Creep rangeCreep = new Creep(new Texture("creep.png"));		// Need picture of range creep
		rangeCreep.sprite.setPosition(2000,2000);				// Need to set positions of 1 range creeps
		rangeCreep.sprite.setOrigin(Constants.CREEP_WIDTH/2, Constants.CREEP_HEIGHT/2);
		Pixmap attackCircle = new Pixmap(101, 101, Pixmap.Format.RGBA4444);
		attackCircle.setColor(Color.RED);
		attackCircle.drawCircle(50,50,50);
		attackingRadius = new Radius(new Texture(attackCircle),rangeCreep.sprite.getX(), rangeCreep.sprite.getY(), 50);
		Pixmap sightCircle = new Pixmap(401, 401, Pixmap.Format.RGBA4444);
		sightCircle.setColor(Color.ORANGE);
		sightCircle.drawCircle(200,200,200);
		sightRadius = new Radius(new Texture(sightCircle),rangeCreep.sprite.getX(), rangeCreep.sprite.getY(), 200);
		drawableObjects.add(rangeCreep);
		drawableObjects.add(attackingRadius);
		drawableObjects.add(sightRadius);*/
	}
	
	private void initLanes(){
		Border right = new Border(200,1945,1850,280);
		Border left= new Border(10,1855,1740,120);
		drawableObjects.add(right);
		drawableObjects.add(left);
	}
	
	
	private void initInhibitor(){
		// Creates bottom left sides inhibitor
		Inhibitor inhibitor = new Inhibitor(new Texture("inhibitor-p.png"));
		inhibitor.sprite.setPosition(((float)2/16 * Constants.MAP_WIDTH) , ((float)2/16 * Constants.MAP_HEIGHT));
		inhibitor.sprite.setOrigin(Constants.INHIBITOR_WIDTH/2, Constants.INHIBITOR_HEIGHT/2);
		inhibitor.setIsTeam(true);
		drawableObjects.add(inhibitor);
		
		// Creates top right sides inhibitor
		Inhibitor inhibitor2 = new Inhibitor(new Texture("inhibitor-p.png"));
		inhibitor2.sprite.setPosition(((float)14/16 * Constants.MAP_WIDTH) , ((float)14/16 * Constants.MAP_HEIGHT));
		inhibitor2.sprite.setOrigin(Constants.INHIBITOR_WIDTH/2, Constants.INHIBITOR_HEIGHT/2);
		inhibitor2.setIsTeam(false);
		drawableObjects.add(inhibitor2);
	}
	
	private void initNexus(){
		// Creates bottom left sides Nexus
		Nexus nexus = new Nexus(new Texture("Nexus.png"));
		nexus.sprite.setPosition(((float)1/32 * Constants.MAP_WIDTH) , ((float)1/32 * Constants.MAP_HEIGHT));
		nexus.sprite.setOrigin(Constants.NEXUS_WIDTH/2, Constants.NEXUS_HEIGHT/2);
		nexus.setIsTeam(true);
		drawableObjects.add(nexus);
		
		// Creates top right sides Nexus
		Nexus nexus2 = new Nexus(new Texture("Nexus.png"));
		nexus2.sprite.setPosition(((float)31/32 * Constants.MAP_WIDTH) , ((float)61/64 * Constants.MAP_HEIGHT));
		nexus2.sprite.setOrigin(Constants.NEXUS_WIDTH/2, Constants.NEXUS_HEIGHT/2);
		nexus2.setIsTeam(false);
		drawableObjects.add(nexus2);
	}
	
	private void initTurret(){
		int turnum = 4;
		for(int i = 1; i < 16; i += 2){
			Turret turret;
			if(i < 8){
				turret = new Turret(new Texture("SouthTurret2.png"), turnum);
				turret.setIsTeam(true);
				turnum--;
			}
			else{
				turnum++;
				turret = new Turret(new Texture("NorthTurret2.png"), turnum);
				turret.setIsTeam(false);
			}
			
			// Set some condition to set 8 positions of turrets
			
			turret.sprite.setPosition(((float)i/16 * Constants.MAP_WIDTH) , ((float)i/16 * Constants.MAP_HEIGHT));
			turret.sprite.setOrigin(Constants.TURRET_WIDTH/2, Constants.TURRET_HEIGHT/2);
			drawableObjects.add(turret);
		}
	}
	
	
	private void initSkill(float x, float y, boolean actorOrAI){
		if(actorOrAI){
			Pixmap circle = new Pixmap(151, 151, Pixmap.Format.RGBA4444);
			circle.setColor(Color.RED);
			circle.drawCircle(75, 75, 75);
			circle.fillCircle(75, 75, 75);
			skillHit = false;
			skillTimer = 0;
			drawableObjects.add(new Skills(new Texture(circle), x, y));
		}
		if(!actorOrAI){
			Pixmap circle = new Pixmap(301, 301, Pixmap.Format.RGBA4444);
			circle.setColor(Color.BLUE);
			circle.drawCircle(150, 150, 150);
			aiSkill = true;
			drawableObjects.add(new Skills(new Texture(circle), x-75, y-75));
		}
	}
	private void initSound(){
		footSound = Gdx.audio.newSound(Gdx.files.internal("255569__stradie__footstep-slow2.wav"));
		backgroundNoise = Gdx.audio.newMusic(Gdx.files.internal("84565__nandoo1__nandoo-moonlight-pad.wav"));
		backgroundNoise.setLooping(true);
		backgroundNoise.play();
		backgroundNoise.setVolume(0.2f);
	}
	
	public void update(){
		if(!gameOver){
			processMouseInput();
			processKeyboardInput();
			
			deltaT = Gdx.graphics.getDeltaTime();
			
			actorStatus();
			actor.update(deltaT);
			//actorAI.update(deltaT);
			
			for(int i = 0; i < drawableObjects.size(); i++){
				GameObject gObj = drawableObjects.get(i);
				
				if(gObj instanceof Actor){
					if(isDead){
						gameOver = true;
						defeat = true;
					}
						//actor.respawn();
				}
				
				if(gObj instanceof ActorAI){
					((ActorAI) gObj).update(deltaT);
					if(actor.sprite.getBoundingRectangle().setSize(70).overlaps(((ActorAI) gObj).sprite.getBoundingRectangle()
							.setSize(270).setCenter(((ActorAI) gObj).sprite.getX()+37.5f, ((ActorAI) gObj).sprite.getY()+37.5f))){
						initSkill(((ActorAI) gObj).sprite.getX(), ((ActorAI) gObj).sprite.getY(),false);
						actor.actorHP -= .9;
					}
					
				}								
				if(gObj instanceof Creep){
					//int ind = creeps.indexOf(((Creep) gObj));
					
					((Creep) gObj).update(deltaT);
					
					if(!gObj.getTeam()){
						if(((Creep) gObj).getHP() <= 0 ){	// Remove creep when it dies
							drawableObjects.remove(i);
							creepNum--;
							actor.gainExp(100);
							actor.changeSpeed(Constants.ACTOR_BASE_SPEED);
						}
						
						for(GameObject gObjC : drawableObjects){
							if(gObjC instanceof Creep){
								if(actor.sprite.getBoundingRectangle().overlaps(((Creep) gObjC).sprite.getBoundingRectangle())){
									((Creep) gObjC).changeSpeed(0);
									actor.changeSpeed(1);
									actor.actorHP -= .01;							
									((Creep) gObjC).updateHP(.1f);	
									break;
								}
								else{
									((Creep) gObjC).changeSpeed(Constants.CREEP_BASE_SPEED);
									actor.changeSpeed(Constants.ACTOR_BASE_SPEED);
								}
							}
						}
						
						for(int j = drawableObjects.size()-1; j >= 0; j--){			// Check for instances of skills hitting the creep
							if(drawableObjects.get(j) instanceof Skills){
								GameObject gObjS = drawableObjects.get(j);
								if(((Skills) gObjS).sprite.getBoundingRectangle().overlaps(((Creep) gObj).sprite.getBoundingRectangle()) && !skillHit){		// If skill hits creep
									skillHit = true;			// Used to remove skill object from screen since it hit already
									((Creep) gObj).updateHP(25);
									System.out.println(((Creep) gObj).getHP());
									break;
								}
								else{
									((Creep) gObj).changeSpeed(Constants.CREEP_BASE_SPEED);	
									actor.changeSpeed(Constants.ACTOR_BASE_SPEED);
								}
							}						
						}
					}
				}
				
				if(gObj instanceof Inhibitor){
					((Inhibitor) gObj).update(deltaT);

					if(!gObj.getTeam()){
						if(actor.sprite.getBoundingRectangle().overlaps(((Inhibitor) gObj).sprite.getBoundingRectangle())){
							actor.changeSpeed(1);
							//actor.actorHP -= .5;							
							((Inhibitor) gObj).updateHP(.5f);
						}
						else{
							//((Inhibitor) gObj).changeSpeed(Constants.CREEP_BASE_SPEED);	
							//actor.changeSpeed(5);
						}
						
						if(((Inhibitor) gObj).getHP() <= 0 )	{	// Remove Inhibitor when it dies
							drawableObjects.remove(i);
							//turret.remove(((Inhibitor) gObj));
							actor.gainExp(300);
							actor.changeSpeed(5);
						}
					}
				}
				
				if(gObj instanceof Nexus){
					((Nexus) gObj).update(deltaT);
					
					if(gObj.getTeam()){
						if(((Nexus) gObj).getHP() <= 0){
							drawableObjects.remove(i);
							gameOver = true;
							victory = false;
						}
					}

					if(!gObj.getTeam()){
						if(actor.sprite.getBoundingRectangle().overlaps(((Nexus) gObj).sprite.getBoundingRectangle())){
							//((Nexus) gObj).changeSpeed(0);
							actor.changeSpeed(1);
							//actor.actorHP -= .5;							
							((Nexus) gObj).updateHP(.5f);
						}
						else{
							//((Nexus) gObj).changeSpeed(Constants.CREEP_BASE_SPEED);	
							//actor.changeSpeed(5);
						}
						
						if(((Nexus) gObj).getHP() <= 0 ){	// Remove Nexus when it dies
							drawableObjects.remove(i);
							//turret.remove(((Nexus) gObj));
							gameOver = true;
							victory = true;
						}
					}
				}
				
				if(gObj instanceof Turret){
					((Turret) gObj).update(deltaT);
					
					if(!gObj.getTeam()){
						if(actor.sprite.getBoundingRectangle().overlaps(((Turret) gObj).sprite.getBoundingRectangle())){
							//((Turret) gObj).changeSpeed(0);
							actor.changeSpeed(1);
							actor.actorHP -= .5;							
							((Turret) gObj).updateHP(.5f);
						}
						else{
							//((Turret) gObj).changeSpeed(Constants.CREEP_BASE_SPEED);	
							//actor.changeSpeed(5);
						}
						
						if(((Turret) gObj).getHP() <= 0 )	{	// Remove Turret when it dies
							drawableObjects.remove(i);
							//turret.remove(((Turret) gObj));
							actor.gainExp(300);
							actor.changeSpeed(5);
						}
					}
				}
				
				if(gObj instanceof Skills){
					//((Skills) gObj).update(deltaT);
					if(skillTimer >= 0.25 && !aiSkill){
						drawableObjects.remove(i);
					}
					else{
						aiSkill = false;
					}
					/*
					if(creep.sprite.getBoundingRectangle().overlaps(((Skills)gObj).sprite.getBoundingRectangle())){
						if(creep.creepHP > 0){
							creep.creepHP = ((Skills)gObj).returnDmg(creep.creepHP);
							drawableObject.remove(i);
							creep.update(Gdx.graphics.getDeltaTime());
						}
						if(creep.creepHP <= 0)
							drawableObjects.remove(creep);
					}
					*/
				}
				if(gObj instanceof Radius){
					((Radius) gObj).update(deltaT);
				}
			}
			if(skillTimer >= 2)
				skillTimer = 2;
			else
				skillTimer += deltaT;
		}
	}
	
	private void processKeyboardInput(){
        if (Gdx.input.isKeyPressed(Keys.ESCAPE)) ; // Might want to pause the game
        
        actorAngle();
        
        if(Gdx.input.isKeyJustPressed(Keys.Q)){
        	if(!isDead && !noMana && skillTimer == 2){
        		initSkill(actor.sprite.getX(), actor.sprite.getY(), true);
        		actor.actorMP -= 10;
        	}
        } 
	}

	private void processMouseInput(){
        if(Gdx.input.isButtonPressed(Input.Buttons.LEFT));		// Selecting game objects, kinda like highlighting them and showing their stats
        if(Gdx.input.isButtonPressed(Input.Buttons.RIGHT));		// Move character to that position, if clicked on ally move towards ally, if clicked on enemy attacks enemy	        
        
        if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
        	processActorUpdates();
        	processCamera();
        }
	}
	
	public void processActorUpdates(){
    	//System.out.println("(LINE) X: " + actor.sprite.getX() + " Y: " + actor.sprite.getY());
    	newMouseX = camera.getCam().position.x - (screenWidth/2) + Gdx.input.getX();
    	newMouseY = camera.getCam().position.y - (screenHeight/2) + (screenHeight - Gdx.input.getY());
    	//System.out.println("CAMERA POSITION X: " + camera.getCam().position.x + " Y: " + camera.getCam().position.y);
    	//System.out.println("MOUSE POSITION X: " + newMouseX + " Y: " + newMouseY);
    	actor.face(new Vector2(newMouseX-actor.sprite.getX() - 37, -(newMouseY-actor.sprite.getY() - 50)));
    	//actor.getTargetDirection();
    	//System.out.println(actor.length());
    	if(actor.length() > 10){
    		actor.moveTo();
    		//System.out.println("ACTOR POSITION: " + actor.position);
    		footSound.play(.1f);
    	}
	}
	
	public void processCamera(){
		camX = actor.sprite.getX();
		camY = actor.sprite.getY();
		camera.getCam().position.set(camX, camY, camZ);
		
		if(camera.getCam().position.x <= 0 + screenWidth/2)
			camX = screenWidth/2;
		if(camera.getCam().position.x >= Constants.MAP_WIDTH - screenWidth/2)
			camX = Constants.MAP_WIDTH - screenWidth/2;
		if(camera.getCam().position.y <= 0 + screenHeight/2)
			camY = screenHeight/2;
		if(camera.getCam().position.y >= Constants.MAP_HEIGHT - screenHeight/2)
			camY = Constants.MAP_HEIGHT - screenHeight/2;
		if(camX != actor.sprite.getX() || camY != actor.sprite.getY())
			camera.getCam().position.set(camX, camY, camZ);
	}
	
	public void callActor(){
		initActor();
		initActorAI();
	}
	
	public void actorStatus(){
		if(actor.actorHP == 0)
			isDead = true;
		if(actor.actorMP == 0)
			noMana = true;
	}
	
	public void actorAngle(){
		//angle = Math.atan2(-(screenHeight - Gdx.input.getY() - actor.getPosition().y - 37.5f), Gdx.input.getX() - actor.getPosition().x - 37.5f);
		angle = Math.atan2(-(newMouseY-actor.sprite.getY() - 37), newMouseX-actor.sprite.getX() - 37);
		angle = angle * (180/Math.PI);
        if(angle < 0){
            angle = 360 - (-angle);
        }
        
        if(angle>135 && angle <225){
            num = 1;
        }
        
        if(angle>0 && angle<45)
        	num = 2;
        if(angle>315 && angle <360)
            num = 2;
        
        if(angle>225 && angle <315){
            num = 3;
        }
        
        if(angle >45 && angle < 135){
            num = 4;
        }
	}
	
	public float getActorX(){
		return actor.sprite.getX();
	}
	
	public float getActorY(){
		return actor.sprite.getY();
	}
	
	public float getActorHP(){
		return actor.actorHP;
	}
	
	public float getActorMP(){
		return actor.actorMP;
	}
	
	public int getActorLvl(){
		return actor.currentLevel;
	}
	
	public int getDirection(){
		return num;
	}
	
	public float getNewMouseX(){
		return camera.getCam().position.x - (screenWidth/2);
	}
	
	public float getNewMouseY(){
		return camera.getCam().position.y - (screenHeight/2);
	}
	
	public float getSkillTimer(){
		return skillTimer;
	}

	public ArrayList<GameObject> getDrawableObjects() {
		return drawableObjects;
	}
	
	public void dispose(){
		// Dispose of all sprites, sounds, etc
	}
}
