package game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import gameobjects.Actor;
import gameobjects.Creep;
import gameobjects.GameObject;
import gameobjects.Inhibitor;
import gameobjects.Nexus;
import gameobjects.Turret;

public class Renderer{
	SpriteBatch spriteBatch;
    private Controller control;
    private GameScreen camera;
    BitmapFont font;
    Texture gameScreen, gameOverScreen;
	private Animation actorAnim;
	private Animation auraAnim;
	private TextureRegion [] actorFrames;
	private TextureRegion [] auraFrames;
	private TextureRegion [] actorFramesUp;
	private TextureRegion [] actorFramesDown;
	private TextureRegion [] actorFramesLeft;
	private TextureRegion [] actorFramesRight;
	private TextureRegion currentActorFrame;
	private TextureRegion currentAuraFrame;
	float actorAnimStateTime;
	float auraAnimStateTime;
	private int num;

    
    public Renderer(Controller c, GameScreen cam){
    	control = c;
    	camera = cam;
        spriteBatch = new SpriteBatch(); 
        font = new BitmapFont();
        createGameScreen();
        createActor();
        actorAnimStateTime = 0f;
        auraAnimStateTime = 0f;
    }
    
    public void render(){
    	spriteBatch.setProjectionMatrix(camera.getCam().combined);
        spriteBatch.begin();
    	renderGameScreen();
        
	    for(GameObject gObj : control.getDrawableObjects()){
	    	if(gObj instanceof Actor);				// Don't draw the texture of the actor since we don't want it to overlap with the animations
	    	else{
	    		if(gObj instanceof Creep){
		    		//font.draw(spriteBatch, "Creep : " + (int)gObj.getHP(), control.getNewMouseX() + 200, control.getNewMouseY() + 50);
		    		font.draw(spriteBatch, "(" + (int)gObj.getHP() + "/100)", gObj.sprite.getX() + 5, gObj.sprite.getY() + 75);
	    		}
	    		else if(gObj instanceof Inhibitor){
	    			font.draw(spriteBatch, "(" + (int)gObj.getHP() + "/2000)", gObj.sprite.getX() + 15, gObj.sprite.getY() + 125);
	    		}
	    		else if(gObj instanceof Nexus){
	    			font.draw(spriteBatch, "(" + (int)gObj.getHP() + "/2000)", gObj.sprite.getX() + 15, gObj.sprite.getY() + 125);
	    		}
	    		else if(gObj instanceof Turret){
	    			font.draw(spriteBatch, "(" + (int)gObj.getHP() + "/1000)", gObj.sprite.getX() + 10, gObj.sprite.getY() + 115);
	    		}
	    		gObj.sprite.draw(spriteBatch);
	    	}
	    }
	    
		font.draw(spriteBatch, "Health : " + (int)control.getActorHP(), control.getNewMouseX() + 50, control.getNewMouseY() + 75);
		font.draw(spriteBatch, "Mana : " + control.getActorMP(), control.getNewMouseX() + 50, control.getNewMouseY() + 50);
		font.draw(spriteBatch, "Current Level : " + control.getActorLvl(), control.getNewMouseX() + 50, control.getNewMouseY() + 25);
		font.draw(spriteBatch, "(" + (int)control.getActorHP() + "/250)", control.actor.sprite.getX() + 10, control.actor.sprite.getY() + 135);
		font.draw(spriteBatch, "Skill Cooldown : " + control.getSkillTimer(), control.getNewMouseX() + 200, control.getNewMouseY() + 25);
		
	    renderActor();
    		
    spriteBatch.end();
    }
    
    public void createGameScreen(){
    	gameScreen = new Texture("Map6400.png");
    }
    
    public void renderGameScreen(){
    	spriteBatch.draw(gameScreen, 0, 0);
    }
    
    public void createActor(){			// Set actor frames
       	actorFrames = control.actorPic.actorFrames;
       	auraFrames = control.actorPic.auraFrames;
       	
		actorFramesUp = new TextureRegion[4];
		actorFramesUp[0] = actorFrames[12];
		actorFramesUp[1] = actorFrames[13];
		actorFramesUp[2] = actorFrames[14];
		actorFramesUp[3] = actorFrames[15];
		
		actorFramesDown = new TextureRegion[4];
		actorFramesDown[0] = actorFrames[0];
		actorFramesDown[1] = actorFrames[1];
		actorFramesDown[2] = actorFrames[2];
		actorFramesDown[3] = actorFrames[3];
		
		actorFramesLeft = new TextureRegion[4];
		actorFramesLeft[0] = actorFrames[4];
		actorFramesLeft[1] = actorFrames[5];
		actorFramesLeft[2] = actorFrames[6];
		actorFramesLeft[3] = actorFrames[7];
		
		actorFramesRight = new TextureRegion[4];
		actorFramesRight[0] = actorFrames[8];
		actorFramesRight[1] = actorFrames[9];
		actorFramesRight[2] = actorFrames[10];
		actorFramesRight[3] = actorFrames[11];
    }
    
    public void renderActor(){				// Draw actor frames
    	num = control.getDirection();
    	
    	auraAnim = new Animation(0.06f, auraFrames);
		auraAnimStateTime += Gdx.graphics.getDeltaTime();
		currentAuraFrame = auraAnim.getKeyFrame(auraAnimStateTime, true);
		spriteBatch.draw(currentAuraFrame, control.getActorX()-35, control.getActorY()-10, 150,150);
    	
    	if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
	        switch(num){
		        case 1: actorAnim = new Animation(0.08f, actorFramesLeft);
		        		break;
		        case 2: actorAnim = new Animation(0.08f, actorFramesRight);
		        		break;
		        case 3: actorAnim = new Animation(0.08f, actorFramesUp);
		        		break;
		        case 4: actorAnim = new Animation(0.08f, actorFramesDown);
		        		break;
	        }			
		
	        actorAnimStateTime += Gdx.graphics.getDeltaTime();

	        currentActorFrame = actorAnim.getKeyFrame(actorAnimStateTime, true);

	        spriteBatch.draw(currentActorFrame, control.getActorX(), control.getActorY());
    	}
    	else{
    		switch(num){
		        case 1: spriteBatch.draw(actorFrames[4], control.getActorX(), control.getActorY());
		        		break;
		        case 2: spriteBatch.draw(actorFrames[8], control.getActorX(), control.getActorY());
		        		break;
		        case 3: spriteBatch.draw(actorFrames[12], control.getActorX(), control.getActorY());
		        		break;
		        case 4: spriteBatch.draw(actorFrames[0], control.getActorX(), control.getActorY());
		        		break;
    		}	
    	}
    }
    
	public void dispose(){
		spriteBatch.dispose();
		font.dispose();
	}
}
