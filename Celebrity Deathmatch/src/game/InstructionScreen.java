package game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.CDMGame;

public class InstructionScreen implements Screen, InputProcessor{
	CDMGame game;
	BitmapFont font;
	WelcomeScreen wScreen;
	private SpriteBatch spriteBatch;
	private Texture characterSelectionScreen, backButton;
	int screenWidth, screenHeight;
	
	public InstructionScreen(CDMGame g){
		super();
		font = new BitmapFont();
		Gdx.input.setInputProcessor(this);
		game = g;
		screenWidth = Constants.GAME_WINDOW_WIDTH;
		screenHeight = Constants.GAME_WINDOW_HEIGHT;
		spriteBatch = new SpriteBatch(); 
		characterSelectionScreen = new Texture("temporarycharselectionscreen.jpg");	// Background image for character selection screen
		backButton = new Texture("back_button-128.png");					// Back Button image
	}
	
	@Override
	public void dispose() {
		font.dispose();
		spriteBatch.dispose();
		characterSelectionScreen.dispose();
		backButton.dispose();
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void render(float arg0) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		spriteBatch.begin();
		spriteBatch.draw(characterSelectionScreen, 0, 0, screenWidth, screenHeight);		// Screen cleared from WelcomeScreen but we still want the same background
		spriteBatch.draw(backButton, 0, screenHeight - backButton.getHeight());				// Draw back button to get back to the Welcome Screen
    	font.draw(spriteBatch, "Instructions", screenWidth/2 - 100, screenHeight);			// Draw text
        font.drawMultiLine(spriteBatch, "To play the game, first choose a character to play with a left click.\n"		// Draw text of instructions on how to play
        		+ "Once you selected a character, it will start the game.\n"
        		+ "There are two teams, your team and the enemy AI team.\n"
        		+ "You will control one character while the AI controls the other character.\n"
        		+ "The goal of the game is to destroy the enemies main structure which is called the Nexus.\n"
        		+ "You will have creeps that will spawn regularly to help you destroy the enemy base.\n"
        		+ "You will aim for the enemies Nexus while the enemy will do the same for your Nexus.\n"
        		+ "Beware, as each team has turrets that defend against intruders. You have to destroy them to safely move on.\n"
        		+ "Once either Nexus reaches 0 HP, the game will end and you can either quit the game or play again.\n"
        		+ "To move your character, hold the left mouse button down.\n"
        		+ "To do regular attacks, you must be touching the enemy and both your HP and their HP will start to go down.\n"
        		+ "To use skills, press Q. There is a cooldown time for the skill so you can't spam it.\n"
        		+ "Have fun!", screenWidth/2 - 300, screenHeight-50);
		spriteBatch.end();
	}

	@Override
	public void resize(int arg0, int arg1) {
	}

	@Override
	public void resume() {
	}

	@Override
	public void show() {
	}

	@Override
	public boolean keyDown(int arg0) {
		return false;
	}

	@Override
	public boolean keyTyped(char arg0) {
		return false;
	}

	@Override
	public boolean keyUp(int arg0) {
		return false;
	}

	@Override
	public boolean mouseMoved(int arg0, int arg1) {
		return false;
	}

	@Override
	public boolean scrolled(int arg0) {
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int arg3) {
		if(Gdx.input.getX() > 0 && Gdx.input.getX() < backButton.getWidth() && Gdx.input.getY() > 0 && Gdx.input.getY() < backButton.getHeight()){	// If back button picture is clicked
			wScreen = (WelcomeScreen)game.getWelcomeScreen();
			game.setScreen(wScreen);
			wScreen.setIP();				// Sets input back to the welcome screen since we're reusing the Screen object instead of making a new one
			dispose();
		}
		return false;
	}

	@Override
	public boolean touchDragged(int arg0, int arg1, int arg2) {
		return false;
	}

	@Override
	public boolean touchUp(int arg0, int arg1, int arg2, int arg3) {
		return false;
	}

}