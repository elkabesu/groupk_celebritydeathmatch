package game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class GameScreen implements Screen, InputProcessor{

	private Controller control; 
	private Renderer render;
	private OrthographicCamera cam;
	
	CDMGame game;
	
	public GameScreen(CDMGame g, boolean Neo, boolean ChuckNorris){
		super();
		control = new Controller(this);
		if(Neo){			// Used for changing which sprite to display
			control.actorPic = new RenderActor(2);
			control.actorAIPic = new RenderActor(1);
		}
		else{
			control.actorPic = new RenderActor(1);
			control.actorAIPic = new RenderActor(2);
		}
    	control.callActor();
    	render = new Renderer(control, this); 
		Gdx.input.setInputProcessor(this);
		game = g;
		//cam = new OrthographicCamera(30, 30 * (Gdx.graphics.getHeight()/Gdx.graphics.getWidth()));
		//cam = new OrthographicCamera(Constants.GAME_WINDOW_WIDTH, Constants.GAME_WINDOW_HEIGHT);
		cam = new OrthographicCamera(1280, 720);
		cam.position.set(cam.viewportWidth/2f, cam.viewportHeight/2f, 0);			// Set camera to look at the center of the screen
		cam.update();
	}
	
	@Override
	public void dispose() {
		control.dispose();
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void render(float arg0) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		cam.update();
		control.update();
		render.render();
		if(control.gameOver){
			if(control.victory)			// Call GameOverScreen with condition as you won the game, else condition is you lost
				game.setScreen(new GameOverScreen(game, true));
			else
				game.setScreen(new GameOverScreen(game, false));
		}
		
	}

	@Override
	public void resize(int arg0, int arg1) {
	}

	@Override
	public void resume() {
	}

	@Override
	public void show() {
	}

	@Override
	public boolean keyDown(int arg0) {
		return false;
	}

	@Override
	public boolean keyTyped(char arg0) {
		return false;
	}

	@Override
	public boolean keyUp(int arg0) {
		return false;
	}

	@Override
	public boolean mouseMoved(int arg0, int arg1) {
		return false;
	}

	@Override
	public boolean scrolled(int arg0) {
		return false;
	}

	@Override
	public boolean touchDown(int arg0, int arg1, int arg2, int arg3) {
		return false;
	}
	
	@Override
	public boolean touchDragged(int arg0, int arg1, int arg2) {
		return false;
	}

	@Override
	public boolean touchUp(int arg0, int arg1, int arg2, int arg3) {
		return false;
	}
	
	public OrthographicCamera getCam(){
		return cam;
	}
}
