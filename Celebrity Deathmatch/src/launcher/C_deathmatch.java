package launcher;

import game.CDMGame;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class C_deathmatch {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "Celebrity Deathmatch";
        config.width = 1280;
        config.height = 720;
        config.resizable = false;
        new LwjglApplication(new CDMGame(), config);
    }
}